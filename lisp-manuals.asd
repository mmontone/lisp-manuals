(asdf:defsystem #:lisp-manuals
  :description "Collection of lisp manuals served by webinfo"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
	       (:file "lisp-manuals"))
  :depends-on (:webinfo))

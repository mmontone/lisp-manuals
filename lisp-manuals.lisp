(in-package :lisp-manuals)

(defparameter *lisp-manuals-repository*
  (make-instance 'source-filesystem-info-repository
                 :source-directory (asdf:system-relative-pathname :lisp-manuals "manuals/")
                 :working-directory (ensure-directories-exist
				     (merge-pathnames ".local/share/lisp-manuals/" (user-homedir-pathname)))))

(defun start (&rest args)
  (apply #'webinfo:start-webinfo
	 :info-repository
	 *lisp-manuals-repository*
	 :app-settings (list (cons :theme (make-instance 'webinfo:nav-theme)))
	 args))

;;(webinfo::compile-texi-files *lisp-manuals-repository*)
;;(dir *lisp-manuals-repository*)
;;(mapcar 'indexes (dir *lisp-manuals-repository*))

;; (create-index-file *lisp-manuals-repository*)
;;(read-index "djula:compile-template*" :fn *lisp-manuals-repository*)

